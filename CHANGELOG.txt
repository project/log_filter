log_filter 7.x-1.0.x, 2013-03-26
--------------------------------
* Removed Inspect module dependency.

log_filter 7.x-1.0.x, 2013-02-09
--------------------------------
* Frontend now strips tags off text fields.

log_filter 7.x-1.0.x, 2013-02-05
--------------------------------
* Finished message box; .showAll() working, and fully documented.

log_filter 7.x-1.0.x, 2013-02-03
--------------------------------
* Frontend now uses inspect.errorHandler().
* Implemented message box.

log_filter 7.x-1.0.x, 2013-02-02
--------------------------------
* Filter styling done.
* Implemented time fields.

log_filter 7.x-1.0.x, 2013-01-28
--------------------------------
* em-styling to get rid of naseously complex javascript resizing.

log_filter 7.x-1.0.x, 2013-01-27
--------------------------------
* Filter selector is no longer read-only during create/edit; user is allowed to change filter even in these modes (previous behaviour was too restrictive, instead let the user makes his mistakes and learn).
* Changing filter to default no longer fires a page refresh.
* Implemented 2 update list buttons.
* Lots of visual tweaking.

log_filter 7.x-1.0.x, 2013-01-27
--------------------------------
* Delete logs: don't prompt user for confirmation again if there's a limit, if already prompted for confirmation whilst there was no limit.
* Create filter: names 'default' and 'adhoc' are illegal.
* Worked on setting filter name as url parameter.

log_filter 7.x-1.0.x, 2013-01-13
--------------------------------
* Added role condition.
* Put validation on more condition fields.
* Create filter done.
* Apparantly fixed frontend mode bugs.
* Added delete max. field.
* Log deletion only allowed upon fresh log list update; the log list must reflect the filter.
* Made the first steps toward log deletion - establishing current criteria and setting up confirm()s.

log_filter 7.x-1.0.x, 2013-01-06
--------------------------------
* Removed create mode from viewer form, because that mode is handled solely via AJAX.
* Frontend almost ready, except mode and name get wrong when alternating between create, edit, cancel etc.

log_filter 7.x-1.0.x, 2013-01-05
--------------------------------
* Declared menu items now optionally hide default dblog viewer/remover page.
* Frontend modes seem fully resolved.

log_filter 7.x-1.0.x, 2013-01-04
--------------------------------
* Fronted now use elements directly for DOM and event modifications, instead of css selectors (the selectors only used initially for getting the elements).

log_filter 7.x-1.0.x, 2012-12-16
---------------------------
* Project started.